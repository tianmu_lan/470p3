gui_state_default_create -off -ini

# Globals
gui_set_state_value -category Globals -key recent_sessions -value {{gui_load_session -ignore_errors -file /home/tianmu/Desktop/470p2/p2part3/DVEfiles/dve_checkpoint_15358_1.tcl}}

# Layout
gui_set_state_value -category Layout -key child_console_size_x -value 2479
gui_set_state_value -category Layout -key child_console_size_y -value 171
gui_set_state_value -category Layout -key child_data_coltype -value 65
gui_set_state_value -category Layout -key child_data_colvalue -value 122
gui_set_state_value -category Layout -key child_data_colvariable -value 241
gui_set_state_value -category Layout -key child_data_size_x -value 431
gui_set_state_value -category Layout -key child_data_size_y -value 1042
gui_set_state_value -category Layout -key child_driver_size_x -value 2207
gui_set_state_value -category Layout -key child_driver_size_y -value 179
gui_set_state_value -category Layout -key child_hier_col3 -value {-1}
gui_set_state_value -category Layout -key child_hier_colpd -value 0
gui_set_state_value -category Layout -key child_hier_size_x -value 105
gui_set_state_value -category Layout -key child_hier_size_y -value 1042
gui_set_state_value -category Layout -key child_hier_sort_order -value 1
gui_set_state_value -category Layout -key child_source_docknewline -value false
gui_set_state_value -category Layout -key child_source_pos_x -value {-2}
gui_set_state_value -category Layout -key child_source_pos_y -value {-15}
gui_set_state_value -category Layout -key child_source_size_x -value 1946
gui_set_state_value -category Layout -key child_source_size_y -value 1037
gui_set_state_value -category Layout -key child_wave_colname -value 351
gui_set_state_value -category Layout -key child_wave_colvalue -value 351
gui_set_state_value -category Layout -key child_wave_left -value 706
gui_set_state_value -category Layout -key child_wave_right -value 1722
gui_set_state_value -category Layout -key main_pos_x -value 48
gui_set_state_value -category Layout -key main_pos_y -value 117
gui_set_state_value -category Layout -key main_size_x -value 2527
gui_set_state_value -category Layout -key main_size_y -value 1413
gui_set_state_value -category Layout -key stand_wave_child_docknewline -value false
gui_set_state_value -category Layout -key stand_wave_child_pos_x -value {-2}
gui_set_state_value -category Layout -key stand_wave_child_pos_y -value {-15}
gui_set_state_value -category Layout -key stand_wave_child_size_x -value 2438
gui_set_state_value -category Layout -key stand_wave_child_size_y -value 1213
gui_set_state_value -category Layout -key stand_wave_top_pos_x -value 20
gui_set_state_value -category Layout -key stand_wave_top_pos_y -value 120
gui_set_state_value -category Layout -key stand_wave_top_size_x -value 2453
gui_set_state_value -category Layout -key stand_wave_top_size_y -value 1413

# list_value_column

# Sim

# Assertion

# Stream

# Data

# TBGUI

# Driver

# Class

# Member

# ObjectBrowser

# UVM

# Local

# Backtrace

# FastSearch

# Exclusion

# SaveSession

# FindDialog
gui_create_state_key -category FindDialog -key m_pMatchCase -value_type bool -value false
gui_create_state_key -category FindDialog -key m_pMatchWord -value_type bool -value false
gui_create_state_key -category FindDialog -key m_pUseCombo -value_type string -value {}
gui_create_state_key -category FindDialog -key m_pWrapAround -value_type bool -value true

# Widget_History
gui_create_state_key -category Widget_History -key {dlgSimSetup|m_setupTab|tab pages|SimTab|m_VPDCombo} -value_type string -value inter.vpd
gui_create_state_key -category Widget_History -key {dlgSimSetup|m_setupTab|tab pages|SimTab|m_argsCombo} -value_type string -value {{-ucligui +v2k +vc +memcbk} {+v2k +vc +memcbk}}
gui_create_state_key -category Widget_History -key {dlgSimSetup|m_setupTab|tab pages|SimTab|m_curDirCombo} -value_type string -value /home/tianmu/Desktop/470p2/p2part3
gui_create_state_key -category Widget_History -key {dlgSimSetup|m_setupTab|tab pages|SimTab|m_exeCombo} -value_type string -value {/home/tianmu/Desktop/470p2/p2part3/dve dve}


gui_state_default_create -off
